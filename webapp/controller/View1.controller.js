sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function (Controller, JSONModel) {
	"use strict";

	return Controller.extend("com.capgemini.coe.REAssetsTree.controller.View1", {
		onInit: function () {
			this.dataModel = this.getOwnerComponent().getModel();
			this.getData();
		},

		getData: function () {
			this.dataModel.read("/REASSETVIEW", {
				success: function (oData, oResponse) {
					this.getCategoryData(oData.results);
				}.bind(this),
				error: function (error) {}
			});
		},

		getCategoryData: function (masterData) {
			this.dataModel.read("/RECATEGORY", {
				success: function (oData, oResponse) {
					this.getSubCategoryData(masterData, oData.results);
				}.bind(this),
				error: function (error) {}
			});
		},

		getSubCategoryData: function (masterData, categoryData) {
			this.dataModel.read("/RESUBCATEGORY", {
				success: function (oData, oResponse) {
					this.getAssetsData(masterData, categoryData, oData.results);
				}.bind(this),
				error: function (error) {}
			});
		},

		getAssetsData: function (masterData, categoryData, subCategoryData) {
			this.dataModel.read("/REASSET", {
				success: function (oData, oResponse) {
					this.arrangeData(masterData, categoryData, subCategoryData, oData.results);
				}.bind(this),
				error: function (error) {}
			});
		},

		arrangeData: function (masterData, categoryData, subCategoryData, assetData) {
			var parentNode = {};
			var treeData = [];
			var treeCategoryData = [];
			var treeSubCategoryData = [];
			var treeAssetData = [];
			for (var i = 0; i < masterData.length; i++) {
				parentNode = {};
				parentNode.id = masterData[i].id;
				parentNode.title = masterData[i].Title;
				var categoryNode = {};
				treeCategoryData = [];
				for (var j = 0; j < categoryData.length; j++) {
					categoryNode = {};
					if (parentNode.id === categoryData[j].Master_id) {
						categoryNode.id = categoryData[j].id;
						categoryNode.title = categoryData[j].Category;
						treeCategoryData.push(categoryNode);
					}
					var subCategoryNode = {};
					treeSubCategoryData = [];
					for (var k = 0; k < subCategoryData.length; k++) {
						subCategoryNode = {};
						if (subCategoryData[k].Category_id === categoryNode.id) {
							subCategoryNode.id = subCategoryData[k].id;
							subCategoryNode.title = subCategoryData[k].Sub_Category;
							treeSubCategoryData.push(subCategoryNode);
						}
						var assetNode = {};
						treeAssetData = [];
						for (var m = 0; m < assetData.length; m++) {
							assetNode = {};
							if (assetData[m].Sub_Category_id === subCategoryNode.id) {
								assetNode.id = assetData[m].id;
								assetNode.title = assetData[m].Asset;
								assetNode.link = assetData[m].Link;
								treeAssetData.push(assetNode);
							}
						}
						subCategoryNode.categories = treeAssetData;
					}
					categoryNode.categories = treeSubCategoryData;
				}
				parentNode.categories = treeCategoryData;
				treeData.push(parentNode);
			}
			var nodeModel = new JSONModel(treeData);
			this.getView().setModel(nodeModel, "nodeModel");
			debugger;
		}
	});
});